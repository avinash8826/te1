package login

import grails.transaction.Transactional
@Transactional(readOnly = true)
class UserController {
	static allowedMethods = [signup:"GET" , save: "POST"]
	def index() { 
	}
	def signup() { 
	}
	def home() { 
	}
	def login(){
		def user = User.findByUsernameAndPassword(params.username, params.password)
		if(user){
		  session.user = user
		  flash.message = "Hello do you krsna is the supreme personality of godhead ${user.username}!"
		  redirect(action:"home")      
		}else{
		  flash.message = "Error"
		  redirect(action:"index")
		}
	}
	@Transactional
	def save() {
		if (params == null) { 
			flash.message = ''
			return
		}
		def user = User.findByUsername(params.username);
		if(user != null){
			flash.message = "Username already exist"
			render(view:"signup")
		} else{
			def newUser = new User();
			newUser.username = params.username
			newUser.email = params.email
			newUser.password = params.password
			if(newUser.save(flush:true)) {
				flash.message = "User Created. Please Login"
				render(view:"index") 
			} else {			
				flash.message = "Please enter valid data";
				render(view:"signup") 
			} 
		}
	}
}
