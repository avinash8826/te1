<%@ page import="login.User" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
</head>
<body>   
	<div class="container">    
		<g:if test="${flash.message}">
			<h1>${flash.message}</h1> 
		</g:if>  
	</div>
</body> 
</html>
 